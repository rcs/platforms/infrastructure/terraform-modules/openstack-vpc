resource "openstack_networking_network_v2" "network" {
  name           = var.network_name
  description    = var.network_description
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet" {
  name            = "${var.network_name}-subnet"
  network_id      = openstack_networking_network_v2.network.id
  cidr            = var.subnet_cidr
  ip_version      = 4
  dns_nameservers = var.dns_nameservers
}

resource "openstack_networking_router_v2" "router" {
  name                = "${var.network_name}-router"
  admin_state_up      = "true"
  external_network_id = data.openstack_networking_network_v2.external_network.id
}

resource "openstack_networking_router_interface_v2" "interface" {
  router_id = openstack_networking_router_v2.router.id
  subnet_id = openstack_networking_subnet_v2.subnet.id
}

data "openstack_networking_network_v2" "external_network" {
  name = var.external_network_name
}

# Wait few seconds after creating the router to avoid problems with
# the instances using this resource
resource "null_resource" "before" {
  depends_on = [openstack_networking_router_interface_v2.interface]
}

resource "null_resource" "delay" {
  provisioner "local-exec" {
    command = "sleep 10"
  }
  triggers = {
    "before" = null_resource.before.id
  }
}

resource "null_resource" "after" {
  depends_on = [null_resource.delay]
}

resource "openstack_networking_secgroup_v2" "network_security_groups" {
  count       = length(var.sec_groups)
  name        = lookup(var.sec_groups[count.index], "name", "")
  description = lookup(var.sec_groups[count.index], "description", "")
}
