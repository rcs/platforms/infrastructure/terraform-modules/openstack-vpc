variable "dns_nameservers" {
  type        = list(string)
  default     = ["131.111.8.42", "131.111.12.20"]
  description = "CUDN DNS Nameservers"
}

variable "network_name" {
  type        = string
  default     = "network"
  description = "Name of network"
}

variable "network_description" {
  type        = string
  default     = "network description"
  description = "Brief description of network"
}

variable "subnet_cidr" {
  type        = string
  default     = "10.10.0.0/16"
  description = "CIDR for the subnet"
}

variable "external_network_uuid" {
  type        = string
  default     = ""
  description = "UUID of the external network"
}
variable "external_network_name" {
  type        = string
  default     = ""
  description = "Name of the external network"
}

variable "enable_gateway" {
  type        = string
  default     = "true"
  description = " Enable gateway on the subnet created in this network."
}

variable "fip_pool" {
  type        = string
  default     = ""
  description = "Floating IP pool. Should match with UUID of external network"
}

variable "metadata" {
  type        = map(string)
  description = "Map containing all meta-data like prefix, environment, project ID"
}


variable "sec_groups" {
  type    = list
  default = []

}
