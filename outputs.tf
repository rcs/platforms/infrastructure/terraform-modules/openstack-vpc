output "id" {
  value = openstack_networking_network_v2.network.id
}
output "name" {
  value = openstack_networking_network_v2.network.name
}
output "subnet_id" {
  value = openstack_networking_subnet_v2.subnet.id
}

output "sec_groups" {
  # value = ["${openstack_networking_floatingip_v2.fip.*.address}"]
  value = [
    var.sec_groups == "" ? join(",", openstack_networking_secgroup_v2.network_security_groups.*.id) : length(var.sec_groups)
  ]
}

output "network" {
  value = "${merge(
    map("name", openstack_networking_network_v2.network.name),
    map("id", openstack_networking_network_v2.network.id),
    map("subnet_id", openstack_networking_subnet_v2.subnet.id),
    map("router_external_ip", lookup(openstack_networking_router_v2.router.external_fixed_ip[0], "ip_address")),
    # map("cidr", lookup(local.networks, "internal"))
    map("cidr", var.subnet_cidr)
  )}"

}

output "sec_group_map" {
  value = zipmap(openstack_networking_secgroup_v2.network_security_groups.*.name, openstack_networking_secgroup_v2.network_security_groups.*.id)
}
