locals {
  data = {
    "environment" = var.environment
    "name"        = var.stack_name
  }
}

module "internal_network" {
  source              = "../"
  metadata            = "${local.data}"
  network_name        = "internal-network"
  network_description = "Used by nodes to communicate"
  subnet_cidr         = "10.0.0.0/16"
  #   Enable gateway on the subnet created in this module. 
  #   Defaults to true
  enable_gateway = "true"
  #   CUDN-Private or CUDN-Internet
  external_network_name = "CUDN-Internet"
  #   Optional security group configuration
  #   These groups are not instance specific, they apply to a whole
  #   network
  sec_groups = [
    {
      name        = "monitoring group"
      description = "Prometheus Monitoring"
    },
  ]
}
