output "network_id" {
  value = module.internal_network.id
}
output "security_groups" {
  value = module.internal_network.sec_groups
}

