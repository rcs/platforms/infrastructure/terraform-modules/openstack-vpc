variable "environment" {
  type        = string
  default     = "staging"
  description = "Your Environment"
}
variable "stack_name" {
  type        = string
  default     = "stacky"
  description = "Name of your stack"
}
